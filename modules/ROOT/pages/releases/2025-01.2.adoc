= Testing Farm release 2025-01.2
:keywords: Releases

Testing Farm release 2025-01.1 was deployed on 2025-02-05 and release 2025-01.2 on 2025-02-27 🎉.
This page contains release notes for both versions

== 🚀 Upgrades

xref:cli.adoc[Testing Farm CLI] tool was updated to https://gitlab.com/testing-farm/cli/-/releases/v0.0.23[`v0.0.23`], please update your installation.

https://gitlab.com/testing-farm/artemis/[Artemis] was updated to https://gitlab.com/testing-farm/artemis/-/releases/v0.0.74[`v0.0.74`] on both ranches.

https://tmt.readthedocs.org[tmt] was updated to https://tmt.readthedocs.io/en/stable/releases.html#tmt-1-43[`1.43`].

== 🎯 Highlights

Added support for RHEL Image Mode images in Testing Farm (AWS, Openstack) (https://issues.redhat.com/browse/TFT-2787[TFT-2787]).
See xref:rfd/rfd5-testing-image-mode.adoc#_composes[documentation] for list of supported composes.

Implemented first version of xref:rfd/rfd2-testing-farm-repo-config.adoc[RFD2 - Testing Farm Repository Configuration] (https://issues.redhat.com/browse/TFT-2472[TFT-2472]) with first version including secrets support.
See xref:test-request.adoc#secrets-in-repo-config[documentation] on how to use it.

The `testing-farm` CLI is now packaged and available in copr repositories https://copr.fedorainfracloud.org/coprs/g/testing-farm/stable/package/testing-farm/[@testing-farm/stable] and https://copr.fedorainfracloud.org/coprs/g/testing-farm/latest/package/testing-farm/[@testing-farm/latest] (https://issues.redhat.com/browse/TFT-2801[TFT-2801]).

Added support for system reservation after testing (https://issues.redhat.com/browse/TFT-2291[TFT-2291]).

== 🚀 Improvements

Added tmt subresults to TF's results.xml (https://issues.redhat.com/browse/TFT-2841[TFT-2841]).

Onboarded RHEL-10.0 image mode to Testing Farm (https://issues.redhat.com/browse/TFT-3092[TFT-3092]).

Onboarded Fedora-42 (https://issues.redhat.com/browse/TFT-3121[TFT-3121]).

Add list of files uploaded to by the test as links on results.xml (https://issues.redhat.com/browse/TFT-2645[TFT-2645]).

Extend CLI option --redhat-brew-build so it can also consume NVR (https://issues.redhat.com/browse/TFT-2880[TFT-2880]).

Add "whoami" query on the API (https://issues.redhat.com/browse/TFT-2537[TFT-2537]).

Add Artemis driver for Power infra (ppc64) (https://issues.redhat.com/browse/TFT-2876[TFT-2876]).

Add support for expanding phases in oculus (https://issues.redhat.com/browse/TFT-2930[TFT-2930]).

Skipped test suite should be visible in results.xml and oculus (https://issues.redhat.com/browse/TFT-2869[TFT-2869]).

Display result of each check and subresult in oculus (https://gitlab.com/testing-farm/oculus/-/merge_requests/78[oculus!78]).

Worker image option is now narrowed down to specific images (https://issues.redhat.com/browse/TFT-2976[TFT-2976]).

== 📃 Documentation

Added example how to easily enable `sos-report` in the finish step (https://gitlab.com/testing-farm/docs/root/-/merge_requests/169[docs!169]).

== 🐞 Bugfixes

Fix CentOS 10 images having Fedora 40 repository (https://issues.redhat.com/browse/TFT-2564[TFT-2564]).

Fix inconsistent test result between API request and artifacts web page (https://issues.redhat.com/browse/TFT-2660[TFT-2660]).

Add repository normalization to present consistent naming and content (https://issues.redhat.com/browse/TFT-2763[TFT-2763]).

Fix guest event log is gone from artifacts (https://issues.redhat.com/browse/TFT-2874[TFT-2874]).

Fix parallel archiving stopping working (https://issues.redhat.com/browse/TFT-2917[TFT-2917]).

Fix incorrect error: Ansible playbook execution (https://issues.redhat.com/browse/TFT-2439[TFT-2439]).

Fix missing test results not triggering error (https://issues.redhat.com/browse/TFT-3021[TFT-3021]).

Fix stuck threads in worker (https://issues.redhat.com/browse/TFT-3086[TFT-3086]).

Fix submitting a request without test.sti or test.fmf causing API outage (https://issues.redhat.com/browse/TFT-3102[TFT-3102]).

Fix testing-farm restart not honoring tmt path (https://issues.redhat.com/browse/TFT-2719[TFT-2719]).

Fix compose building of some rhel-8 and rhel-9 images (https://issues.redhat.com/browse/TFT-3009[TFT-3009]).

Fix rendering skipped checks in oculus (https://issues.redhat.com/browse/TFT-3103[TFT-3103]).

Fix rendering notes as a list in oculus (https://gitlab.com/testing-farm/oculus/-/merge_requests/75[oculus!75]).

Fix rendering empty subresults in oculus (https://gitlab.com/testing-farm/oculus/-/merge_requests/78[oculus!78]).

== 📦️ Packages

List of important packages bundled in the worker image.

[source,shell]
....
❯ podman run --entrypoint rpm quay.io/testing-farm/worker-public:2025-01.2 -q fmf tmt standard-test-roles ansible-core podman beakerlib | sort | uniq
ansible-core-2.16.14-1.fc40.noarch
beakerlib-1.31.4-1.fc40.noarch
fmf-1.6.1-1.fc40.noarch
podman-5.3.1-1.fc40.x86_64
standard-test-roles-4.11-3.fc40.noarch
tmt-1.43.0-1.fc40.noarch
....

== 🔎 Stats

Testing Farm is on track to surpass https://stats.testing-farm.io/d/dpYooDIVk/testing-farm-all-time-stats[1.8M requests per year] \o/.

The average error rate for Testing Farm was approximately http://metrics.osci.redhat.com/d/NnHWU1dnz/testing-farm?orgId=1&refresh=1m&from=now-30d&to=now&viewPanel=12[4.06%].
