= Testing Farm release 2023-03.1
:keywords: Releases

Testing Farm is happy to be back with its second named release 2023-03.1 🎉.

== Incompatible Changes

* We had to update to Ansible 2.14 from the pre-historic version 2.9 we used for a longer time.
  Due to this, STI tests or prepare playbooks might break if `command` or `shell` was used with `warn: false`.
  The `warn` option for these modules was removed, so make sure to fix your tests or playbooks.
  An Example of a fix for the podman can be found https://src.fedoraproject.org/rpms/podman/pull-request/54#request_diff[here].

== Upgrades

* https://tmt.readthedocs.io/en/stable/[tmt] updated from 1.19 to https://github.com/teemtee/tmt/releases/tag/1.21.0[1.21]
  (see also release notes for https://github.com/teemtee/tmt/releases/tag/1.20.0[1.20] and https://github.com/teemtee/tmt/releases/tag/1.21.0[1.21])

* Artemis upgrade to https://gitlab.com/testing-farm/artemis/-/releases/v0.0.54[v0.0.54], from a prehistoric version, this supports multiple enhancements in the release.

* Ansible upgrades from 2.9 to 2.14, see <<Incompatible Changes>>

=== Improvements

* A lot of new pages have landed into our https://docs.testing-farm.io[User Documentation]

* Improved https://docs.testing-farm.io/Testing%20Farm/0.1/test-results.html#_reproducer[tmt reproducer] steps with artifact installation (thanks to Martin Pitt / Cockpit)

* Results viewer now displays the https://docs.testing-farm.io/Testing%20Farm/0.1/test-results.html#_errors[error reason] in the plan error summary

* https://docs.testing-farm.io/Testing%20Farm/0.1/test-request.html#hardware[Hardware requirements] are now supported in Public Ranch

* Support for Polarion results exporter https://issues.redhat.com/browse/TFT-1503[TFT-1503]

* Cloud costs reporting - Instances are now tagged with the Testing Farm user name and tags can be adjusted in the API https://issues.redhat.com/browse/TFT-1890[TFT-1890]
  (A separate announcement is coming for this feature)

== Bugfixes

* Fixed Testing Farm CLI issue waiting endlessly for very large plans (https://issues.redhat.com/browse/TFT-1738[TFT-1738])
* Testing Farm CLI fixes support for `--hardware virtualization.is-virtualized=false` (https://issues.redhat.com/browse/TFT-1807[TFT-1807])
* Updated CentOS Stream9 image to latest (https://issues.redhat.com/browse/TFT-1757[TFT-1757])
* Fixed handling of `excludes` for `install` prepare step, to exclude certain packages from installation (https://issues.redhat.com/browse/TFT-1541[TFT-1541])
* rhpkg is now available on the worker, bringing better support for `standard-test-source` role (https://issues.redhat.com/browse/TFT-1748[TFT-1748])

== Packages

List of important packages bundled in the worker image.

[source,shell]
....
❯ podman run --entrypoint rpm quay.io/testing-farm/worker:2023-03.1 -q tmt standard-test-roles ansible podman beakerlib | sort | uniq
ansible-core-2.14.2-2.el8.x86_64
beakerlib-1.29.3-1.el8.noarch
podman-4.3.1-2.module_el8.8.0+1254+78119b6e.x86_64
standard-test-roles-4.10-6.el8.noarch
tmt-1.21.0-1.el8.noarch
....

== Statistics

* Error rate ~ 3.09% in the last 30 days
* ~ 52k testing requests in the last 30 days
