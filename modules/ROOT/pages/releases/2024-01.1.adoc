= Testing Farm release 2024-01.1
:keywords: Releases

Testing Farm release 2024-01.1 is deployed 🎉.


== 💔 Breaking Changes

We have enhanced the security of our endpoints. As a result, restarting requests that you do not own is no longer possible. Please inform us if this change causes any issues for you.

Test environment variable `TESTING_FARM_USERNAME` has been temporarily removed from the testing environment. We believe it is not in use; however, if you are utilizing it, we will restore it as outlined in https://issues.redhat.com/browse/TFT-2433[TFT-2433]


== 🚀 Upgrades

xref:cli.adoc[Testing Farm CLI] tool updated from `v0.0.15` to `v0.0.16`, please update your installation.


== 🎯 Highlights

Authorization of API endpoints has been re-implemented using https://casbin.org/[casbin] framework. (https://issues.redhat.com/browse/TFT-2275[TFT-2275])

RHEL-7.9-Nightly compose has been added to Testing Farm and will be updated weekly. It targets the latest nightly updated composes. (https://issues.redhat.com/browse/TFT-2390[TFT-2390])

The i686 packages are now available alongside x86_64 in the xref:test-environment.adoc#_tag_repository[tag repository]. (https://issues.redhat.com/browse/TFT-2090[TFT-2090])

Testing Farm CLI has been updated to support the use of the `install: false` flag for artifacts. (https://issues.redhat.com/browse/TFT-2322[TFT-2322])


== 🐞 Bugfixes

A regression in our xref:test-results.adoc#_results_viewer[Results Viewer] that caused all errors to be hidden when some of the plans ended in an error state has been fixed. (https://issues.redhat.com/browse/TFT-2409[TFT-2409])

Resolved the issue of a blank page appearing in certain cases in our xref:test-results.adoc#_results_viewer[Results Viewer]. (https://issues.redhat.com/browse/TFT-2414[TFT-2414])

Tmt environment variables are no longer hidden as secrets when they are not sensitive. (https://issues.redhat.com/browse/TFT-2416[TFT-2416])

Corrected the `rhel-extras.repo` to accurately point to RHEL-7.9-Nightly instead of the `rel-eng` compose. (https://issues.redhat.com/browse/TFT-2410[TFT-2410])


== 📦️ Packages

List of important packages bundled in the worker image.

[source,shell]
....
❯ podman run --entrypoint rpm quay.io/testing-farm/worker:2024-01.1 -q tmt standard-test-roles ansible-core podman beakerlib | sort | uniq
ansible-core-2.14.11-2.fc38.noarch
beakerlib-1.29.3-2.fc38.noarch
podman-4.8.3-1.fc38.x86_64
standard-test-roles-4.11-2.fc38.noarch
tmt-1.30.0-1.fc38.noarch
....

== 🔎 Stats

Testing Farm is on track to surpass https://stats.testing-farm.io/d/dpYooDIVk/testing-farm-all-time-stats[950k requests per year] \o/.

Error rate in January was approximately http://metrics.osci.redhat.com/d/NnHWU1dnz/testing-farm?viewPanel=12&orgId=1&from=1704063600000&to=1706569200000[1.79%].

Around http://metrics.osci.redhat.com/d/NnHWU1dnz/testing-farm?viewPanel=8&orgId=1&from=1704063600000&to=1706569200000[76k testing requests] were recorded in January.

Average queue time in January was http://metrics.osci.redhat.com/d/NnHWU1dnz/testing-farm?viewPanel=26&orgId=1&from=1704063600000&to=1706569200000[1.53 minutes on the Public ranch] and http://metrics.osci.redhat.com/d/NnHWU1dnz/testing-farm?viewPanel=27&orgId=1&from=1704063600000&to=1706569200000[2.67 minutes on the Red Hat ranch].
