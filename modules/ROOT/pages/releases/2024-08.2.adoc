= Testing Farm release 2024-08.2
:keywords: Releases

Testing Farm release 2024-08.2 is deployed 🎉.

== 📦️ Packages

List of important packages bundled in the worker image.

[source,shell]
....
❯ podman run --entrypoint rpm quay.io/testing-farm/worker-public:2024-08.2 -q tmt standard-test-roles ansible-core podman beakerlib | sort | uniq
ansible-core-2.16.12-1.fc40.noarch
beakerlib-1.31.2-1.fc40.noarch
podman-5.2.3-1.fc40.x86_64
standard-test-roles-4.11-3.fc40.noarch
tmt-1.38.0-1.fc40.noarch
....
