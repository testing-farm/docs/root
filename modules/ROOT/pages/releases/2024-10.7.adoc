= Testing Farm release 2024-10.7
:keywords: Releases

Testing Farm release 2024-10.7 was deployed on 2025-01-21 🎉.

== 🚀 Upgrades

https://tmt.readthedocs.org[tmt] was updated to https://tmt.readthedocs.io/en/stable/releases.html#tmt-1-41[`1.41`].
https://fmf.readthedocs.org[fmf] was updated to https://fmf.readthedocs.io/en/stable/releases.html#fmf-1-6-1[`1.6.1`].

== 🐞 Bugfixes

Fix issue causing requests to stuck in `running` state and outdated logs. (https://issues.redhat.com/browse/TFT-3086[TFT-3086])

== 📦️ Packages

List of important packages bundled in the worker image.

[source,shell]
....
❯ podman run --entrypoint rpm quay.io/testing-farm/worker-public:2024-10.7 -q fmf tmt standard-test-roles ansible-core podman beakerlib | sort | uniq
ansible-core-2.16.14-1.fc40.noarch
beakerlib-1.31.3-1.fc40.noarch
fmf-1.6.1-1.fc40.noarch
podman-5.3.1-1.fc40.x86_64
standard-test-roles-4.11-3.fc40.noarch
tmt-1.41.0-1.fc40.noarch
....
