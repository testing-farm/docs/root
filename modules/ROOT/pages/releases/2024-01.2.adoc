= Testing Farm release 2024-01.2
:keywords: Releases

Testing Farm release 2024-01.2 is deployed 🎉.


== 🚀 Upgrades

* https://tmt.readthedocs.io/en/stable/[tmt] updated from 1.30 to https://tmt.readthedocs.io/en/stable/releases.html#tmt-1-31[1.31].


== 🎯 Highlights

Fedora 40 is now supported on Testing Farm. `Fedora-latest` points to Fedora 39.

The https://docs.testing-farm.io/Testing%20Farm/0.1/test-results.html#console-log[Console log] is now archived during test execution. The link to the log is not available yet in the results viewer. To access it, copy the link to the workdir from the `pipeline.log`. In the next release we will provide an easier way to access the log. (https://issues.redhat.com/browse/TFT-2289[TFT-2289])


== 🐞 Bugfixes

Fix an issue when using variables in `tmt`'s `discover` step where test environment varilables were not correctly expanded when used. (https://issues.redhat.com/browse/TFT-2192[TFT-2192])

Rsync is now using timeout to mitigate some corner cases with archiving. This should improve the stability of the archiving. (https://issues.redhat.com/browse/TFT-2192[TFT-2192])


== 📦️ Packages

List of important packages bundled in the worker image.

[source,shell]
....
❯ podman run --entrypoint rpm quay.io/testing-farm/worker:2024-01.2 -q tmt standard-test-roles ansible-core podman beakerlib | sort | uniq
ansible-core-2.14.11-2.fc38.noarch
beakerlib-1.29.3-2.fc38.noarch
podman-4.8.3-1.fc38.x86_64
standard-test-roles-4.11-2.fc38.noarch
tmt-1.31.0-1.fc38.noarch
....
