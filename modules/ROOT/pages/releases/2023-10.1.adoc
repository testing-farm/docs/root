= Testing Farm release 2023-10.1
:keywords: Releases

Testing Farm release https://issues.redhat.com/projects/TFT/versions/12411756[2023-10.1] is deployed 🎉.


== Upgrades

* https://tmt.readthedocs.io/en/stable/[tmt] updated from 1.27.0 to https://github.com/teemtee/tmt/releases/tag/1.28.0[1.28.2].

* https://gitlab.com/testing-farm/artemis[Artemis] upgraded from v0.0.63 to https://gitlab.com/testing-farm/artemis/-/releases/v0.0.65[v0.0.65], see also release notes for https://gitlab.com/testing-farm/artemis/-/releases/v0.0.63[v0.0.63], https://gitlab.com/testing-farm/artemis/-/releases/v0.0.64[v0.0.64].

* Testing Farm CLI tool updated to v0.0.14, please update your installation for the new features to work.


== Highlights

* The initial version of the multihost testing is now available. The testing is opt-in and has some limitations. See xref:test-request.adoc#multihost-testing[our documentation for details]. Our results viewer has been updated to properly display the test environments and results. Happy experimenting! (https://issues.redhat.com/browse/TFT-2165[TFT-2165])

* xref:test-request.adoc#cancel[Request cancellation] in Testing Farm is now possible. Use the `DELETE` method on a request or the `testing-farm cancel` CLI command to cancel a request easily. (https://issues.redhat.com/browse/TFT-1696[TFT-1696])

* The Staging API is now accessible via https://api.staging.testing-farm.io/v0.1/about[api.stage.testing-farm.io], offering a preview of our API reimplementation using the FastAPI framework. (https://issues.redhat.com/browse/TFT-2237[TFT-2237])

* The xref:test-request.adoc#artifact-installation-order[order of artifact installation] can now be altered, addressing some corner cases when installing various builds simultaneously. (https://issues.redhat.com/browse/TFT-2172[TFT-2172])


== Bugfixes

* A bug that prevented the cleanup of resources during pipeline timeouts has been fixed. This was a RHIVOS blocker. (https://issues.redhat.com/browse/TFT-2312[TFT-2312])

* Repositories on Beaker boxes have been unified with standard testing images, such as rhel-CRB, etc. (https://issues.redhat.com/browse/TFT-2228[TFT-2228])

* An appropriate error message is now returned if a user tries to install a `repository` artifact that is too large. (https://issues.redhat.com/browse/TFT-2169[TFT-2169])

* The stability of Beaker provisioning for ppc64 has been improved. (https://issues.redhat.com/browse/TFT-2235[TFT-2235])

* Documented https://docs.testing-farm.io/Testing%20Farm/0.1/test-environment.html#_reserved_directories[reserved directories] used by Testing Farm in the test environment. (https://issues.redhat.com/browse/TFT-1473[TFT-1473])

* Non-printable characters are now properly XML-encoded in jUnit XML output. (https://issues.redhat.com/browse/TFT-2046[TFT-2046])

* The transition to sentry-python for error reporting on the Sentry platform from our workers is complete. (https://issues.redhat.com/browse/TFT-2227[TFT-2227])


== Packages

List of important packages bundled in the worker image.

[source,shell]
....
❯ podman run --entrypoint rpm quay.io/testing-farm/worker:2023-10.1 -q tmt standard-test-roles ansible-core podman beakerlib | sort | uniq
ansible-core-2.14.11-1.fc38.noarch
beakerlib-1.29.3-2.fc38.noarch
podman-4.7.2-1.fc38.x86_64
standard-test-roles-4.11-2.fc38.noarch
tmt-1.28.2-1.fc38.noarch
....


== Statistics

* Testing Farm has passed https://stats.testing-farm.io/d/dpYooDIVk/testing-farm-all-time-stats?orgId=1&viewPanel=7[700k requests] per year \o/

* http://metrics.osci.redhat.com/d/NnHWU1dnz/testing-farm?viewPanel=12&orgId=1&from=1696111200000&to=1698793200000[Error rate ~ 3.45% from start of October]

* http://metrics.osci.redhat.com/d/NnHWU1dnz/testing-farm?viewPanel=8&orgId=1&from=1696111200000&to=1698793200000[~ 68k testing requests in October]

* Average queue time http://metrics.osci.redhat.com/d/NnHWU1dnz/testing-farm?viewPanel=26&orgId=1&from=1696111200000&to=1698793200000[30.4s] on Public ranch, http://metrics.osci.redhat.com/d/NnHWU1dnz/testing-farm?viewPanel=27&orgId=1&from=1696111200000&to=1698793200000[53.5s] on Red Hat ranch from the start of October.
