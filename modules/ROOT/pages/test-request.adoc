= Test Request
:keywords: Test Request

== Submitting

Requesting testing is easiest via our xref:cli.adoc[CLI] tool.
You can also directly use our https://api.testing-farm.io/[API].

[#cancel]
== Cancelling

In some cases you want to prematurely cancel your request.
Cancelling is straightforward.

Use the `cancel` xref:cli.adoc[CLI] command and provide the request ID or a string containing it.

.`testing-farm` CLI
[source,shell]
....
$ testing-farm cancel 9baab88b-aca6-4652-ad93-8d954e109a25
$ testing-farm cancel https://api.testing-farm.io/v0.1/requests/a0f18d55-2dd5-466d-b2b8-6bd4a60ca12e
$ testing-farm cancel https://artifacts.dev.testing-farm.io/a0f18d55-2dd5-466d-b2b8-6bd4a60ca12e
....

If you prefer using our https://api.testing-farm.io/[API], submit a request via the `DELETE` method of the `requests` endpoint and pass your token in headers of the request.

.Testing Farm API
[source,json]
....
http -A bearer -a <YOUR_API_KEY> DELETE https://api.testing-farm.io/v0.1/requests/a0f18d55-2dd5-466d-b2b8-6bd4a60ca12e
....

[#reserve]
== Reserve After Testing

Starting with https://docs.testing-farm.io/Testing%20Farm/0.1/releases/2024-10.7.html[release 2025-01.1] Testing Farm can reserve a machine after testing.
This is supported for `request` and `restart` commands.

[NOTE]
====
Currently, reservations with autoconnect are supported only for a single plan.
Restart the request with a single plan if you need reservations.
====

The reservation will insert the test `/testing-farm/reserve-system` from https://gitlab.com/testing-farm/tests into the running plan.

.`testing-farm` CLI
[source,shell]
....
$ testing-farm request --compose Fedora-Rawhide --reserve
$ testing-farm restart --reserve https://artifacts.dev.testing-farm.io/de0f4294-be6d-405f-b41c-15358e5dca57
....

.Testing Farm API
[source,json]
Not supported.

[#plan-filter]
== Tmt Plan Filter
Testing Farm allows to use tmt plan filter.
The feature allows to filter out plans with a help of tmt filtering and regex.
The specified plan filter will be used in `tmt plan ls --filter <YOUR-FILTER>` command.
By default `enabled: true` filter is applied.
See https://tmt.readthedocs.io/en/stable/stories/features.html#filter[tmt documentation] for more information.

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --plan-filter "tag: tier1"
....

.In Testing Farm API:
[source,json]
....
...
{
    "test": {
        "tmt": {
            "plan_filter": "tag: tier1"
        }
    }
}
...
....

[#test-filter]
== Tmt Test Filter
Testing Farm allows to use tmt test filter.
The feature allows to filter out tests in plans with a help of tmt filtering and regex.
The specified plan filter will be used in `tmt run discover plan test --filter <YOUR-FILTER>` command.
See https://tmt.readthedocs.io/en/stable/stories/features.html#filter[tmt documentation] for more information.

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --test-filter "tag: tier1"
....

.In Testing Farm API:
[source,json]
....
...
{
    "test": {
        "tmt": {
            "test_filter": "tag: tier1"
        }
    }
}
...
....

[#hardware]
== Hardware Requirements

Testing Farm allows users to define hardware requirements for the testing environment.
These hardware requirements are used to provision appropriate resources on supported infrastructures.

[NOTE]
====
The xref:cli.adoc[CLI] examples are shortened for brevity and concentrate only on the hardware selection.
Additional required options will be required if you use them.
====

[WARNING]
====
The hardware selection is currently supported only on the Red Hat Ranch.
Support for Public Ranch is coming in Q2/2023.
====

[#architectures]
=== Architecture

Testing Farm provides an ability to provision a guest with a given architecture.

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --arch x86_64
....

.In Testing Farm API:
[source,json]
....
...
{
    "environments": [{
        "arch": "x86_64"
    }]
}
...
....

[IMPORTANT]
====
In `tmt` plan the `arch` field is ignored and replaced by Testing Farm environments `arch` specification.
We plan to fix this problem in the future.
====

[#hostname]
=== Selection by hostname

Testing Farm provides an ability to provision a guest with a specific hostname. Ability to request a hostname matching a filter is also needed, because of guests of similar nature often sharing (sub)domain.

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --hardware hostname='=~ sheep.+'
....

.In Testing Farm API:
[source,json]
....
...
{
    "environments": [{
        "hardware": {
            "hostname": "=~ sheep.+"
        }
    }]
}
...
....

In `tmt` plan
[source,yaml]
....
provision:
  hardware:
    hostname: "=~ sheep.+"
....

[#ram]
=== RAM size selection

Testing Farm provides an ability to provision a guest with specified amount of RAM. Most often, a specific amount of RAM is needed to accommodate a memory-hungry test, making the minimal requirement the most demanded one.

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --hardware memory='>= 8 GB'
....

.In Testing Farm API:
[source,json]
....
...
{
    "environments": [{
        "hardware": {
            "memory": ">= 8 GB"
        }
    }]
}
...
....

In `tmt` plan
[source,yaml]
....
provision:
  hardware:
    memory: ">= 8 GB"
....

[#disk-size]
=== Disk size selection

Testing Farm provides an ability to provision a guest with specified disk size.
The guest will get the disk size according to one of the suitable flavors:

* 🌍 https://gitlab.com/testing-farm/infrastructure/-/blob/main/terragrunt/environments/production/artemis/config/server.yaml.tftpl#L85[Public Ranch flavors]
* 🎩 https://url.corp.redhat.com/artemis-production-02-flavors-2023-05-02[Red Hat Ranch flavors] (RH only link)

The default disk size is:

* `50 GiB` for 🌍 Public Ranch
* `250 GiB` for 🎩 Red Hat Ranch

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --hardware disk.size='>= 80 GB'
....

.In Testing Farm API:
[source,json]
....
...
{
    "environments": [{
        "hardware": {
            "disk": [{
                "size": ">= 80 GB"
            }]
        }
    }]
}
...
....

In `tmt` plan
[source,yaml]
....
provision:
  hardware:
    disk:
      - size: ">= 80 GB"
....

[#tpm]
=== Selection by TPM version

Testing Farm provides an ability to provision a guest with specified Trusted Platform Module (TPM) version.

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --hardware tpm.version=2
....

.In Testing Farm API:
[source,json]
....
...
{
    "environments": [{
        "hardware": {
            "tpm": {
                "version": "2"
            }
        }
    }]
}
...
....

In `tmt` plan
[source,yaml]
....
provision:
  hardware:
    tpm:
      version: 2
....

[#boot]
=== Selecting systems by their boot method - BIOS

Testing Farm provides an ability to provision a guest supporting a specific boot method.
The most common ones are (legacy) BIOS and UEFI, but some architectures may support their own specific methods as well.

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --hardware boot.method='bios'
....

[source,shell]
....
$ testing-farm request --hardware boot.method='!= bios'
....

.In Testing Farm API:
[source,json]
....
...
{
    "environments": [{
        "hardware": {
            "boot": {
                "method": "bios"
            }
        }
    }]
}
...
....

[source,json]
....
...
{
    "environments": [{
        "hardware": {
            "boot": {
                "method": "!= bios"
            }
        }
    }]
}
...
....

In `tmt` plan
[source,yaml]
....
provision:
  hardware:
    boot:
      method: "!= bios"
....

[#distro]
=== Selecting systems by their compatible distro

Testing Farm provides an ability to provision a guest supporting selected distributions (OS).
It is possible to select a HW that is able to run a list of selected distributions.

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --hardware compatible.distro='rhel-7' --hardware compatible.distro='rhel-8'
....

[WARNING]
====
This functionality is currently broken with CLI.
See the https://gitlab.com/testing-farm/cli/-/issues/6[issue here].
====

.In Testing Farm API:
[source,json]
....
...
{
    "environments": [{
        "hardware": {
            "compatible": {
                "distro": [
                    "rhel-7",
                    "rhel-8"
                ]
            }
        }
    }]
}
...
....

In `tmt` plan
[source,yaml]
....
provision:
  hardware:
    compatible:
      distro:
        - rhel-7
        - rhel-8
....

=== Selection by the model name of processor

Testing Farm provides an ability to provision a guest with a CPU of a particular model name.

[#processor-name]

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --hardware cpu.model-name='=~ Intel Xeon'
....

.In Testing Farm API:
[source,json]
....
...
{
    "environments": [{
        "hardware": {
            "cpu": {
                "model-name": "=~ Intel Xeon"
            }
        }
    }]
}
...
....

In `tmt` plan
[source,yaml]
....
provision:
  hardware:
    cpu:
      model-name: "=~ Intel Xeon"
....

[#processor-model]
=== Selection by the model of processor

Testing Farm provides an ability to provision a guest with a CPU of a particular model.

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --hardware cpu.model='1'
....

.In Testing Farm API:
[source,json]
....
...
{
    "environments": [{
        "hardware": {
            "cpu": {
                "model": "1"
            }
        }
    }]
}
...
....

.In `tmt` plan
[source,yaml]
....
provision:
  hardware:
    cpu:
      model: 1
....

[#processor-number]
=== Selection by the number of processors

Testing Farm provides an ability to provision a guest with a given (minimal) number of logical processors.

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --hardware cpu.processors='>= 4'
....

.In Testing Farm API:
[source,json]
....
...
{
    "environments": [{
        "hardware": {
            "cpu": {
                "processors": ">= 4"
            }
        }
    }]
}
...
....

.In `tmt` plan
[source,yaml]
....
provision:
  hardware:
    cpu:
      processors: ">= 4"
....

[#hypervisor]
=== Selecting virtualized guests by their hypervisor

Testing Farm provides an ability to provision a guest that powered by a particular hypervisor.

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --hardware virtualization.hypervisor="!= hyperv"
....

.In Testing Farm API:
[source,json]
....
...
{
    "environments": [{
        "hardware": {
            "virtualization": {
                "hypervisor": "!= hyperv"
            }
        }
    }]
}
...
....

.In `tmt` plan
[source,yaml]
....
provision:
  hardware:
    virtualization:
      hypervisor: "!= hyperv"
....

[#virtualization]
=== Selecting virtualized and non-virtualized guests

Testing Farm provides an ability to provision a guest either virtualized or definitely not virtualized.

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --hardware virtualization.is-virtualized='false'
....

.In Testing Farm API:
[source,json]
....
...
{
    "environments": [{
        "hardware": {
            "virtualization": {
                "is-virtualized": "false"
            }
        }
    }]
}
...
....

.In `tmt` plan
[source,yaml]
....
provision:
  hardware:
    virtualization:
      is-virtualized: false
....

[#virtualization-supported]
=== Selecting guests with virtualization support

Testing Farm provides an ability to ask for guests which support or do not support virtualization.

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --hardware virtualization.is-supported='true'
....

.In Testing Farm API
[source,json]
....
...
{
    "environments": [{
        "hardware": {
            "virtualization": {
                "is-supported": "true"
            }
        }
    }]
}
...
....

.In `tmt` plan
[source,yaml]
....
provision:
  hardware:
    virtualization:
      is-supported: true
....

[#gpu]
=== Selecting guests with a GPU

Testing Farm provides an ability to ask for guests with a GPU.

[NOTE]
====
GPU selection is currently only supported on the public ranch.
If you want to see it in the `redhat` ranch, please file an https://gitlab.com/testing-farm/general/-/issues[issue].
====

Currently we support selecting these using `gpu.device-name` and `gpu.vendor-name` attributes.

[cols="1,1"]
|===
| `vendor-name` | `device-name`

|NVIDIA
|GK210 (Tesla K80)

|NVIDIA
|GV100 (Tesla V100)
|===

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --hardware gpu.device-name="GK210 (Tesla K80)" --hardware gpu.device-name="NVIDIA"
....

.In Testing Farm API
[source,json]
....
...
{
    "environments": [{
        "hardware": {
            "gpu": {
                "device-name": "GK210 (Tesla K80)",
                "vendor-name": "NVIDIA"
            }
        }
    }]
}
...
....

.In `tmt` plan
[source,yaml]
....
provision:
  hardware:
    gpu:
      device-name: GK210 (Tesla K80)
      vendor-name: NVIDIA
....

== Modifying Tmt Steps

Testing Farm allows modifying `discover`, `prepare` and `finish` tmt step phases.
This can be useful to insert a new phase into the tmt execution or update and existing one.
Multiple modifications are supported per request.

For more information see the https://tmt.readthedocs.io/en/stable/stories/cli.html#multiple-phases[official tmt documentation on multiple phases].

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --tmt-discover="--insert --how fmf --url https://gitlab.com/testing-farm/tests --test /testing-farm/reboot" \
                       --tmt-prepare='--insert --how shell --script "echo prepare1"' \
                       --tmt-prepare='--insert --how shell --script "echo prepare2"' \
                       --tmt-finish='--insert --how shell --script "echo finish"'
....

.In Testing Farm API:
[source,json]
....
...
{
    "environments": [{
        "tmt": {
            "extra_args": {
                "discover": [
                    "--insert --how fmf --url https://gitlab.com/testing-farm/tests --test /testing-farm/reboot"
                ],
                "prepare": [
                    "--insert --how shell --script \"echo prepare1\"",
                    "--insert --how shell --script \"echo prepare2\"",
                ],
                "finish": [
                    "--insert --how shell --script \"echo finish\""
                ],
            }
        }
    }]
}
...
....

[#artifact-installation-order]
== Artifact Installation Order

While installing artifacts, Testing Farm follows the order described below.

* repository-file - `10`
* repository - `20`
* fedora-copr-build - `30`
* redhat-brew-build - `40`
* fedora-koji-build - `50`

You can change the order by setting the `order` parameter of an artifact in the `artifacts` section of the `environments` definition.
The order of the installation is from lowest to the highest.

[#provisioning]
== Post Installation Script

The post installation script provides a way to inject a user defined data to the instance startup.
The usage depends on the used pool.

* For clouds providers `AWS`, `Openstack`, `Azure` and `IBM Cloud` can be a shell script or a cloud init configuration.
* For `Beaker` can be a shell script executed after first boot of the system.

[WARNING]
====
For Public Ranch Testing Farm injects by default the following post install script to unblock the root user from logging into the system.

[source,shell]
----
#!/bin/sh
sed -i 's/.*ssh-rsa/ssh-rsa/' /root/.ssh/authorized_keys
----

If you set a post install script, it will override the default. So please make sure you include this snippet, or a cloud init equivalent, or otherwise Testing Farm will not be able to connect to the instance and error out.
====

[WARNING]
====
Do not use the post install script for testing environment setup.
The errors are hard to discover and can hide the fact the post installation script failed.
====

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --post-install-script="#!/bin/sh\nsed -i 's/.*ssh-rsa/ssh-rsa/' /root/.ssh/authorized_keys"
....

.In Testing Farm API
[source,json]
....
...
{
    "environments": [{
        "settings": {
            "provisioning": {
                "post-install-script": "#!/bin/sh\nsed -i 's/.*ssh-rsa/ssh-rsa/' /root/.ssh/authorized_keys"
            }
        }
    }]
}
...
....

.In `tmt` plan
Not supported.

== Provisioning Tags

You can set additional tags for the resources created by the provisioner.
These tags will be used to tag public cloud instances if the provider supports them.
In case of `Beaker` the tags will be added as task parameters `ARTEMIS_TAG_<key>=<value>`

Testing Farm also supports multiple special purpose tags to adjust the provisioning settings for the xref:test-process.adoc#_overview[testing environment].
See the next sections for details.

== Dedicated Instances

Testing Farm by default uses spot instances, if available, to improve cloud costs.
This can cause issues with long running requests, because the spot instances can be returned early on the provider discretion.
In case the spot instance is returned early, usually the testing errors out while connecting back to the instance via SSH.
If you want to avoid this problem, you can ask for dedicated instances using the following provisioning tag:

* `ArtemisUseSpot` (boolean)

If set to `true`, force usage of spot instances. Provisioning pools that do not supporting spot instances are ignored.
If set to `false`, disallow usage of spot instances. Ignored if spot instances are not supported.
By default, if not specified, spot instances are used if available for the chosen infrastructure.
Spot instances are currently supported only for `AWS` provisioning pools.
Supported only on Red Hat ranch.

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --tag ArtemisUseSpot=false
....

.In Testing Farm API
[source,json]
....
...
{
    "environments": [{
        "settings": {
            "provisioning": {
                "tags": {
                    "ArtemisUseSpot": "false",
                }
            }
        }
    }]
}
...
....

.In `tmt` plan
Not supported.

== Cloud Costs

Testing Farm provides cloud costs reporting, see xref:services.adoc#cloud-costs[Cloud Costs] for details.
You can easily group together your costs by using the following tag:

* `BusinessUnit` (string)

Can be set to any string.
Set by default to the Testing Farm token name when unset.
Supported only on Red Hat ranch.

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --tag BusinessUnit=MyGroup
....

.In Testing Farm API
[source,json]
....
...
{
    "environments": [{
        "settings": {
            "provisioning": {
                "tags": {
                    "BusinessUnit": "MyTeam"
                }
            }
        }
    }]
}
...
....

.In `tmt` plan
Not supported.

== Provisioning Error Retries

Testing Farm by default retries provisioning errors for several hours to deal with infrastructure hiccups.
You can control the enablement of these retries using the following tag:

* `ArtemisOneShotOnly` (boolean)

If set to `true`, do not retry recoverable provisioning errors.
Has an effect once a suitable pool is found.
Routing will always be retried.
If set to `false`, retry recoverable provisioning errors.
By default set to `false`.

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --tag ArtemisOneShotOnly=true
....

.In Testing Farm API
[source,json]
....
...
{
    "environments": [{
        "settings": {
            "provisioning": {
                "tags": {
                    "ArtemisOneShotOnly": "true",
                }
            }
        }
    }]
}
...
....

.In `tmt` plan
Not supported.

[#timeout]
== Pipeline Timeout

Testing Farm allows users to set the timeout for their requests in minutes.
The default is 720 (12 hours).
If a job is expected to take a short amount of time, setting a shorter timeout will benefit the user as they will not wait long to timeout on infrastructure issues for example.

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --timeout=20
....

.In Testing Farm API:
[source,json]
....
...
{
    "settings": {
        "pipeline": {
            "timeout": 20
        }
    }
}
...
....

[#parallelization]
== Parallelization

Testing Farm by default runs plans in parallel.
The maximum number of plans run in parallel is by default set to these values:

* `12` for Public ranch
* `5` for Red Hat ranch

The defaults can be overridden.

.With `testing-farm` CLI
[source,shell]
....
$ testing-farm request --parallel-limit 10
....

.In Testing Farm API:
[source,json]
....
...
{
    "settings": {
        "pipeline": {
            "parallel-limit": 10
        }
    }
}
...
....

[#multihost-testing]
== Multihost Testing

Since xref:releases/2023-10.1.adoc[release 2023-10.1], Testing Farm supports https://tmt.readthedocs.io/en/stable/guide.html#multihost-testing[`tmt` multihost testing].
This feature is introduced as a new Testing Farm pipeline, and it is not used by default.

=== Enable Multihost Pipeline

Multihost pipeline is currently opt-in using a feature flag in the test request.
To enable it, the user has to fill the following field in the request JSON:

[NOTE]
====
The multihost pipeline runs plans with `tmt run provision --update-missing`, updating the image in the plan attribute only if missing.
Ensure plans are correctly written.
====

.Testing Farm API
[source,json]
....
...
{
    "settings": {
        "pipeline": {
            "type": "tmt-multihost"
        }
    }
}
...
....

The xref:cli.adoc[CLI] supports this via the option `--pipeline-type tmt-multihost`, which is available for `request` and `restart` commands:

.`testing-farm` CLI
[source,shell]
....
$ testing-farm request --pipeline-type tmt-multihost --git-url https://gitlab.com/testing-farm/tests --plan /testing-farm/multihost --compose Fedora-Rawhide
$ testing-farm restart --pipeline-type tmt-multihost REQUEST_ID
....

[NOTE]
====
Feel free to submit a request yourself using the command above to try it out!
====

=== Current Limitations

* In the https://api.testing-farm.io[Testing Farm API], the fields `pool`, `hardware`, `artifacts`, `settings`, and `kickstart` from the `environments` are ignored.
* xref:test-process.adoc#preparation[Test environment preparation] is not performed.
* The multihost testing is not available in Public Ranch, use Red Hat Ranch for testing.

[#reporting]
== Tmt Process Environment Variables

Testing Farm supports reporting test results to external systems.

[NOTE]
====
This feature is available only for `tmt` tests.
====

These reporting features are implemented as `tmt` plugins, and Testing Farm only provides a way how to safely pass required environment variables for these plugins to work.
The environment variables are directly passed to the `tmt` process, and are explicitly allowlisted in the Testing Farm configuration.

[WARNING]
====
Do not confuse the `tmt` plugin environment variables with test environment variables.
They are two distinct sets of environment variables for different purposes.
====

To set the `tmt` plugin environment variables in Testing Farm use the https://api.testing-farm.io[API] or the xref:cli.adoc[`testing-farm` CLI] tool.

.Testing Farm API
[source,json]
....
{
  ...
  "environments": [
    {
      "tmt": {
        "environment": {
          "VAR1": "VALUE1",
          "VAR2": "VALUE2",
        }
      }
    }
  ]
  ...
}
....

.`testing-farm` CLI
[source,shell]
....
$ testing-farm request --tmt-environment VAR1=VALUE1 --tmt-environment VAR2=VALUE2 ...
....

[#secrets]
== Test Environment Secrets

Testing Farm supports injecting sensitive environment variables into tmt environment.
The difference from test environment variables is that these variables are censored in all artifacts and logs produced by Testing Farm.

[WARNING]
====
The tests *must* make sure to expose secrets in logs as they are to mitigate revealing them.
Testing Farm uses the `sed` command to replace the secrets in all produced artifacts.
====

Currently there are two methods of injecting secrets into a Testing Farm request.

=== Adding secrets when submitting a new test request

.Testing Farm API
[source,json]
....
{
  ...
  "environments": [
    {
      "secrets": {
          "KEY1": "SECRET_VALUE1",
          "KEY2": "SECRET_VALUE2"
      }
    }
  ]
  ...
}
....

.`testing-farm` CLI
[source,shell]
....
$ testing-farm request --secret KEY1=SECRET_VALUE1 --secret KEY2=SECRET_VALUE2 ...
....

[#secrets-in-repo-config]
=== Adding secrets via in-repository configuration

Since the xref:releases/2025-01.2.adoc[2025-01.2 release] which implements a part of xref:rfd/rfd2-testing-farm-repo-config.adoc[RFD2], credentials can be stored publicly in the tested git repository.
These credentials are encrypted with RSA using a private key that is generated by and stored in Testing Farm.
The RSA key pair is unique for each git repository and each Testing Farm token to avoid the possibility of stealing secrets.

To use this method of secrets injection, user has to perform two steps:

1. Encrypt a secret value using the CLI `encrypt` command:

.`testing-farm` CLI
[source,shell]
....
$ testing-farm encrypt --git-url https://gitlab.com/testing-farm/tests "secret message"
🔒 Encrypting secret for your token in repo https://gitlab.com/testing-farm/tests
"83ba2098-0902-494f-8381-fd33bdd2b3b4,VnH/dLzVFdtgYJqSYYzpLJUNeQqWhna6dNXWBy4NiHJgIDTyt/IdTCPT4uXE1DAyMgJSKMXU5hYL8y+Kogs643E37NDjGRJIU/oMM+EQ80x2GCJsl3XRsfw1Ng7sBeNY4nxdh5SMKm0k1yPc4HPQ15N/VR34ar22WCtXS/DQG6Iuc/3bP9S2e3Wvt470/D5h8DRqfsL2/AdalgpIqmSREE7GmOlXm8kcctgD5Uuo+5Zgh4bgpKSYtCz2EGr8i83bMXW3Mfa2htK803iOjFMJet01cQS3AUETFA2g9/XmeJOHkrPFO9cWzjpadY8H6w8HV4HYtGzjsppsSLZsKPzj7ofn2R67YGX/eUZGcqjygqwWWiIz9DQ1i+hvPmlzFtzByH1pTDEzrNSqFdsg2MlB6Wk4fnZcHzCy3xDVkXJfXgY/No0yFlGPoi2wjNRxdFcnb+bsLQRhGzEGj4G/R84jgvXOzjrDAEcfHoIq4untdt7nbEghF6iYtkevSns1UPSVttnUnEKlkZX26BZoZglQOrrrxl3/s6XRAcmj8/p/uaKqrTcaRzfFgvsts0z6tDNnsToCtFq80UsFZz1li2y+6e2n9OviNHjzgCRmrtQuVJrJUkQwKdx8ybW5dxSXbS+1/q/tF4JdccIKnNViuifggcH0H6n5q39AY2l+gB0TslU="
....

[start=2]
2. Store the encrypted secret value in `.testing-farm.yaml` file in the root of the git repository.

.Contents of `.testing-farm.yaml`
[source,shell]
....
version: 1

environments:
  secrets:
    SECRET_KEY: "83ba2098-0902-494f-8381-fd33bdd2b3b4,VnH/dLzVFdtgYJqSYYzpLJUNeQqWhna6dNXWBy4NiHJgIDTyt/IdTCPT4uXE1DAyMgJSKMXU5hYL8y+Kogs643E37NDjGRJIU/oMM+EQ80x2GCJsl3XRsfw1Ng7sBeNY4nxdh5SMKm0k1yPc4HPQ15N/VR34ar22WCtXS/DQG6Iuc/3bP9S2e3Wvt470/D5h8DRqfsL2/AdalgpIqmSREE7GmOlXm8kcctgD5Uuo+5Zgh4bgpKSYtCz2EGr8i83bMXW3Mfa2htK803iOjFMJet01cQS3AUETFA2g9/XmeJOHkrPFO9cWzjpadY8H6w8HV4HYtGzjsppsSLZsKPzj7ofn2R67YGX/eUZGcqjygqwWWiIz9DQ1i+hvPmlzFtzByH1pTDEzrNSqFdsg2MlB6Wk4fnZcHzCy3xDVkXJfXgY/No0yFlGPoi2wjNRxdFcnb+bsLQRhGzEGj4G/R84jgvXOzjrDAEcfHoIq4untdt7nbEghF6iYtkevSns1UPSVttnUnEKlkZX26BZoZglQOrrrxl3/s6XRAcmj8/p/uaKqrTcaRzfFgvsts0z6tDNnsToCtFq80UsFZz1li2y+6e2n9OviNHjzgCRmrtQuVJrJUkQwKdx8ybW5dxSXbS+1/q/tF4JdccIKnNViuifggcH0H6n5q39AY2l+gB0TslU="
....

[WARNING]
====
Only values of secret keys can be encrypted.
The key should not contain any sensitive information.
====

When the user submits a Testing Farm request with the same token that requested secrets encryption, the secret will be injected into tmt environment.

[NOTE]
====
The steps above described a case where a user submits request on their own behalf.
When a third party is submitting requests on user's behalf, modification applies as described below.
====

==== Encrypting values for use with third party Tokens

If a third party (e.g. Packit) is submitting Testing Farm requests on behalf of user, user needs to provide third party's API Token ID to the `testing-farm encrypt` via the `--token-id` option.

.`testing-farm` CLI
[source,shell]
....
$ testing-farm encrypt --git-url https://gitlab.com/testing-farm/tests --token-id c8adc4cd-6a4d-4d58-acf4-2ef599405f2b "secret message"
🔒 Encrypting secret for token id c8adc4cd-6a4d-4d58-acf4-2ef599405f2b in repo https://gitlab.com/testing-farm/tests
"c8adc4cd-6a4d-4d58-acf4-2ef599405f2b,TsVjDwttR/Icc5NtOsWMjo7K7rxt8Cpnck5MFVueHNF+71aGfI4YS+szbahNvqn/Ecn0do1G8qDKGrIvx6PNKVgDWuK4R04m+QyxDrup3dzBvBsiaEkklhPClKZWHgSCWrtDRJYP/0GXuY/SCBeyURaTy9Uc3M7ne4lLacxpm5zunW7l0u+0I+lhttSOLK6zZ0+bhlkY+HMo0xiqv2OBILZ0FQ+xT2vvH66W8+0GOQgcGuQdZZlRVjTm7SumVGJ9K6aAJ+B/S2OtMqfMxGUtGG8ZBYxWZDeyyoPYnkzxnf9A11uSWU/C6nTISHtQod0ztkn8bVgS2fYogpKsE9qmLO7O0v4XvQE1JBhDAeIEW40v00Uq866BeHOSUaTl79z7PicybBex+TrO4hX5LMqKV60oe1LOlFbuo6hHGoH4HaXmhcKvAPxNu+PpgfxfkJAlGgwMnC7uv/kSVzRvBzRZ73MjECRksTkEDcUnX5L5kwiTTl3PPYDpK/ntzSRvvD64IvmJkZVo9nX6pglZY5DDUSriCju7HINM3qmVNho8d4bEDX9mXl8jZJKxiNUo0tVxqNxdiYNu0nS0BjAlzA8umQKr9p3lI2HpIB1dbS+jdsBp3KDWwxhd106C/430nq1SqUc1in0jQBgFzgUHzis0tL7oIC5k1k3k37r5kFsW/Zs="
....

The secrets will be injected when a request is submitted with an API Token that corresponds to the submitted Token ID.

==== Encrypting values for multiple Tokens

Each secret value is tied to a single Testing Farm Token. If the user has different Testing Farm Tokens or use different services (e.g. Packit Public Token and Packit Red Hat Token) to submit a testing request, then it is needed to create a secret using each of the Tokens. Encrypted values are then passed as a list to the intended secret.

Generate a secret for an another Token with corresponding Token ID.

.`testing-farm` CLI
[source,shell]
....
$ testing-farm-public encrypt --git-url https://gitlab.com/testing-farm/tests --token-id ea2a89aa-6a78-40e0-906e-140f623c45b0 "secret message"
🔒 Encrypting secret for token id ea2a89aa-6a78-40e0-906e-140f623c45b0 for repository https://gitlab.com/testing-farm/tests
"ea2a89aa-6a78-40e0-906e-140f623c45b0,IRFNiZeZ10eu+jG2da+aNufT5/5turs91i2alZwxtOAERAmgaczUH38Q+eJHpPId3llAZRIn18VsrfbpyH2v7kZAdZH/NdPqgnq9xLEP2les9PmDtY5rqniAhbQbdhnQNjU5RlCXxU1OtcmYGf1J8maLNMJ2GEEsg3q3KH3GM2bKb6K8dQ20UvzeXbPR6TzYhG/I2NY3CadwKY7yShIxFbTMZ+MxMNmSD9ODURa7ulfKhsq24w7vEttv5JBdNE+j0MzgVCFgBkKP66lMAIanzTbPAtv5wi2UUn1wwwTabHyW6hf8JHVbn0Kr05NUTPMkLGgciVB7s0uuGQ257nIHypcDFeRfm/vlSm5NqwjRTv0Jp+1DKLr2KpqonrczHIsg7d+rcgpsBOQhyLH03H3w9G6UItFdTudIvFSQkgLmEAOrPqMjk4NwU27y/ss/VLI0cjj+WqIainwpm3AFeCo5BzgG5Yzf1a5AAqaumBgqmKN7XcDaKaBDZ4DOMe+lEYNock09ptZtFrRcYq4EvdtdF+MirggeuhdDkepimrSh1VdbMU3u/+TIt2/9ZnszOT9bhAB2r/9F06KZ/mt4Ee1NOeXsZzHbHRMOkzjZ9vbRky8qEYvUMvOLP/vcye+7x1n9nKob7iGto9ho7UH+P1C8uQcZuJwLaYC9M6uwKWryR90="
....


Use this secret in the `.testing-farm.yaml` file alongside the previously generated secret.

.Contents of `.testing-farm.yaml`
[source,shell]
....
version: 1

environments:
  secrets:
    SECRET_KEY:
      - "c8adc4cd-6a4d-4d58-acf4-2ef599405f2b,TsVjDwttR/Icc5NtOsWMjo7K7rxt8Cpnck5MFVueHNF+71aGfI4YS+szbahNvqn/Ecn0do1G8qDKGrIvx6PNKVgDWuK4R04m+QyxDrup3dzBvBsiaEkklhPClKZWHgSCWrtDRJYP/0GXuY/SCBeyURaTy9Uc3M7ne4lLacxpm5zunW7l0u+0I+lhttSOLK6zZ0+bhlkY+HMo0xiqv2OBILZ0FQ+xT2vvH66W8+0GOQgcGuQdZZlRVjTm7SumVGJ9K6aAJ+B/S2OtMqfMxGUtGG8ZBYxWZDeyyoPYnkzxnf9A11uSWU/C6nTISHtQod0ztkn8bVgS2fYogpKsE9qmLO7O0v4XvQE1JBhDAeIEW40v00Uq866BeHOSUaTl79z7PicybBex+TrO4hX5LMqKV60oe1LOlFbuo6hHGoH4HaXmhcKvAPxNu+PpgfxfkJAlGgwMnC7uv/kSVzRvBzRZ73MjECRksTkEDcUnX5L5kwiTTl3PPYDpK/ntzSRvvD64IvmJkZVo9nX6pglZY5DDUSriCju7HINM3qmVNho8d4bEDX9mXl8jZJKxiNUo0tVxqNxdiYNu0nS0BjAlzA8umQKr9p3lI2HpIB1dbS+jdsBp3KDWwxhd106C/430nq1SqUc1in0jQBgFzgUHzis0tL7oIC5k1k3k37r5kFsW/Zs="
      - "ea2a89aa-6a78-40e0-906e-140f623c45b0,IRFNiZeZ10eu+jG2da+aNufT5/5turs91i2alZwxtOAERAmgaczUH38Q+eJHpPId3llAZRIn18VsrfbpyH2v7kZAdZH/NdPqgnq9xLEP2les9PmDtY5rqniAhbQbdhnQNjU5RlCXxU1OtcmYGf1J8maLNMJ2GEEsg3q3KH3GM2bKb6K8dQ20UvzeXbPR6TzYhG/I2NY3CadwKY7yShIxFbTMZ+MxMNmSD9ODURa7ulfKhsq24w7vEttv5JBdNE+j0MzgVCFgBkKP66lMAIanzTbPAtv5wi2UUn1wwwTabHyW6hf8JHVbn0Kr05NUTPMkLGgciVB7s0uuGQ257nIHypcDFeRfm/vlSm5NqwjRTv0Jp+1DKLr2KpqonrczHIsg7d+rcgpsBOQhyLH03H3w9G6UItFdTudIvFSQkgLmEAOrPqMjk4NwU27y/ss/VLI0cjj+WqIainwpm3AFeCo5BzgG5Yzf1a5AAqaumBgqmKN7XcDaKaBDZ4DOMe+lEYNock09ptZtFrRcYq4EvdtdF+MirggeuhdDkepimrSh1VdbMU3u/+TIt2/9ZnszOT9bhAB2r/9F06KZ/mt4Ee1NOeXsZzHbHRMOkzjZ9vbRky8qEYvUMvOLP/vcye+7x1n9nKob7iGto9ho7UH+P1C8uQcZuJwLaYC9M6uwKWryR90="
....

When a request is submitted using either one of these two Tokens, the secret value `secret message` will be injected into the Testing Environment.
The secret that matches first will be used.

[#polarion]
== Report to Polarion

To report to Polarion make sure your plan has the `polarion` report plugin enabled.

[source,yaml]
....
report:
    how: polarion
....

See for more information the https://tmt.readthedocs.io/en/latest/spec/plans.html#polarion[polarion report plugin documentation].

Specify the following environment variables:

[source,shell]
....
POLARION_REPO=https://polarion.example.com/repo
POLARION_URL=https://polarion.example.com/polarion
POLARION_PROJECT=your-project
POLARION_USERNAME=your-username
POLARION_PASSWORD=your-password
....

[#reportportal]
== Report to ReportPortal

To report to ReportPortal make sure your plan has the `reportportal` report plugin enabled.

[source,yaml]
....
report:
    how: reportportal
    project: your-project
....

See for more information the https://tmt.readthedocs.io/en/latest/spec/plans.html#reportportal[reportportal report plugin documentation].

Specify the following environment variables:

[source,shell]
....
TMT_PLUGIN_REPORT_REPORTPORTAL_URL=https://reportportal.example.com
TMT_PLUGIN_REPORT_REPORTPORTAL_TOKEN=your-token
....

The variables listed below are optional and are typically configured by users directly within their `tmt` metadata:

[source,shell]
....
TMT_PLUGIN_REPORT_REPORTPORTAL_PROJECT
TMT_PLUGIN_REPORT_REPORTPORTAL_LAUNCH
TMT_PLUGIN_REPORT_REPORTPORTAL_LAUNCH_DESCRIPTION
....
