= Packit
:keywords: Packit


== What is Packit?
An open source project aiming to ease the integration of your project with Fedora Linux, CentOS Stream and other distributions.

Packit makes it easy to build a copr build from an upstream project pull request on Github or Gitlab and test its integration against Fedora, CentOS Stream or RHEL.
Packit uses Testing Farm to run all tests.

To learn more about Packit, see their http://packit.dev[official page].


== Who should use Packit?
Upstream projects that want to build RPM packages out of pull requests, commits or releases, and test their code changes easily.


== Use Cases
1. Functional testing of copr builds built from GitHub pull requests or GitLab merge requests, integrated into Fedora, CentOS Stream or RHEL.
2. Functional testing of GitHub pull requests (without a copr build) in GitLab merge requests
3. Testing against internal Red Hat infrastructure.
4. Testing against unreleased Red Hat products.

For example, in the screenshot below you can see how the integration looks like in a merge requests and all the checks that automatically run by Testing Farm.

image::packit_examples.png[]


== Key notes about Packit integration with Testing Farm
* The onboarding is done via Packit, there is no need to have a dedicated access token for Testing Farm.
* There's no requirement to manage your testing infrastructure, as it is provided to you as a service.
* Test results can be accessed directly through the checks associated in the pull request.
* You have the flexibility to execute multiple test jobs.
* There are no restrictions or limits on the number of tests you can run.
* It is not possible to pass secrets to jobs
