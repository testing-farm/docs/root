= Project Management

This page describes details for our project management.

[#dod]
== Definition of Done

* Issue created in https://issues.redhat.com/projects/TFT/[Jira] with a clear description.
* xref:project.adoc#_jira_labels[Labels] added to issue.
* Release versions set on issue - both quarter scope and product release if relevant.
* Work items completed.
* Work products linked to Jira issue (eg: code change, released software, blog, research report).
* xref:project.adoc#_acceptance_criteria[Acceptance Criteria] was met.
* Work products reviewed by Testing Farm team and stakeholders.
* Feedback from review implemented and all stakeholders satisfied.

Please see the xref:contributing.adoc[Contribution Guide] for details about tools, code, and style.

== Jira Issues

Testing Farm is managed via Jira available at https://issues.redhat.com/browse/TFT.
The instance is currently Red Hat only, we are talking about opening up the issues to the public.

All issues must use the following types:

* Epic - for large features, consisting of multiple issues
* Story - for new features and user stories
* Bug - for bug fixes
* Task - for other tasks

Using subtasks is optional.
Subtasks should not set any labels or be in any releases.
In case a subtask should be in release, convert it to a regular issue.

=== Story Points

As the team, we agreed on the following story points estimation for our team.

[cols="1,3,1,2,2"]
|===
|Story Points |Complexity |Risk |Uncertainty |Time Effort

|1
|Task is extremely simple to do.
 There is a minimal amount of work that needs to be done.
|Low.
|None.
 No consultation needed.
|Takes an hour or less.

|4
|Task is simple to do.
 The acceptance criteria are short and can be satisfied with ease.
|Low.
|None.
 No consultation needed.
|Takes a few hours.

|8
|Task is fairly simple, but can have some smaller complex parts.
 The acceptance criteria are short and can be satisfied with ease.
|Medium.
|Little.
 May need consultation.
|Takes a day.

|40
|Task has more complex parts, but should still be manageble.
 The acceptance criteria are larger, but well understood.
 Most probably should be split into subtasks for better tracking.
|High.
|Medium.
 Will need consultation or multiple consultations.
|Takes a week.

|160
|Task is very complex.
 Cannot be delivered as is and must be split into smaller tasks.
|High.
|Team has fuzzy idea how long will it take.
|Takes a month or more months.
|===

=== Estimation

The story points can be set by the ticket assignee or discussed on team meeting in more uncertain cases.

Because our story points are based on a time work estimation (in hours), we estimate using the following principles:

* The time in hours we estimate to spend on activities to resolve the issue, i.e. coding, writing documentation, discussion, getting more information from peers, resolving review comments, investigating and resolving CI issues caused by the change, etc.
* We exclude from the estimate waiting times, i.e. waiting for CI pipelines to finish, waiting for outages, waiting for blocker issues to resolve, waiting for reviews etc.
* The time during these events should be ideally spent on other tasks.

The `Epic` Jira issue type does not have an estimation.
When another Jira issue type is converted to an Epic, the estimation will be removed by Jira automation.

== Acceptance Criteria

Following acceptance criteria is automatically added to all Jira issues:

[code]
....
(off) Feature or Bug Fix is implemented
(off) Deliverables reviewed
(off) Unit tests were created or updated
(off) Integration tests created or updated
(off) Deployed to all environments
(off) Configuration added/changed and cherry-picked to production
(off) Documentation written or updated
(off) Release notes updated
....

Acceptance criteria can be extended with a checklist of subtasks in case you do not want to use subtasks for this.

Following states are allowed for the acceptance criteria items:

[code]
....
(off) not started
(on) started
(/) done
(-) not applicable
....

== Jira Labels

This is a list of agreed labels to be used with our issues.

Service related:

* `BaseOS-CI`
* `TestingFarm`

Workflow related:

* `ErrorBudget`
* `Escalation`
* `KnownIssue`
* `NeedsTriage`
* `NewFeature`
* `Release`

Planning related:

* `BucketCommunity` - Address public issues, support Fedora community, address tmt MR queue, organize TTI mini-summit, presentations, etc.
* `BucketContinuity` - Continuing to run the business, supporting existing onboarded users and improving experience of the service for them.
* `BucketEffectivity` - Improving effectivity of the team, like onboarding new team members and longer term code contributors, improving automation, etc.
* `BucketMigration` - Migrating users to Testing Farm or implementing new features, so they can onboard.
* `BucketStrategic` - Supporting strategic initiatives like Image Mode, RHEL on GitLab, RHEL on Konflux or RHEL AI.
* `BucketTechnicalDebt` - Addressing bugs or technical debt in our code.

Event related:

* `Gathering`
* `DevConf`
* `QECamp`

General components:

* `Composes`
* `Infrastructure`
* `Monitoring`
* `ProdSec`
* `ProjectManagement`
* `ReportPortal`
* `tft-admin`
* `tmt`
* `STI`

Testing Farm components:

* `API`
* `Artemis`
* `Artifacts`
* `CLI`
* `Oculus`
* `Worker`
* `PublicRanch`
* `RedHatRanch`

Gluetool related:

* `Gluetool`
* `GuestSetup`

BaseOS CI components:

* `Covscan`
* `Restraint`
* `IntegrationTests`
* `wow`

Users:

* `convert2rhel`
* `CRC`
* `FedoraCI`
* `ImageBuilder`
* `JavaQE`
* `KernelQE`
* `KVMQE`
* `OSCI`
* `Packit`
* `RHCOS`
* `ImageModeRHEL`
* `RHIVOS`
* `SecureSign`
* `SystemRoles`
* `Upgrades`
* `VirtQE`
* `Zuul`

Cloud related:

* `AWS`
* `Azure`
* `Beaker`
* `GCP`
* `IBMCloud`
* `Openstack`

Extended issue type:

* `BestPractice`
* `BreakingChange`
* `Contribution`
* `Cost`
* `Demo`
* `Documentation`
* `Presentation`
* `Regression`
* `TechnicalDebt`
* `Usability`
* `UserRequest`

Tickets we want to monitor in other jira projects than TFT:

* `TFT`
