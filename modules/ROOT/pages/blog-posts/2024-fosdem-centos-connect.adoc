= CentOS Connect and FOSDEM 2024 Highlights
:keywords: FOSDEM

== Context
In 2024 Testing Farm team member Evgeny Fedin attended https://fosdem.org/2024/[FOSDEM] and https://connect.centos.org/[CentOS Connect] in Brussels. He shared his highlights from the events focusing more on the less popular but not less important CentOS Connect.

image::fosdem-2024.png[]

== CentOS Connect
CentOS Connect is the contributor conference for CentOS, focusing on CentOS Stream, Special Interest Groups, and the entire Enterprise Linux ecosystem.
CentOS Connect at FOSDEM happened February 1-2, 2024.
The event was a great opportunity to meet and talk with the CentOS community and learn about the latest developments in the CentOS ecosystem.
The event was held at the Radisson Collection Hotel, Grand Place Brussels, Belgium.

=== Thursday, February 1

==== https://connect.centos.org/#altimages[Alternative Image SIG - Let's Talk About It]

The talk focused on the development and distribution of alternative images for CentOS, emphasizing the significance of using the CentOS SIG infrastructure for image creation.
The speaker highlighted recent achievements, including resolving a longstanding boot issue with installations and introducing new KDE and GNOME live images.
Future plans include quarterly updates, automation of image builds, and expanding documentation to facilitate community contributions.

==== https://connect.centos.org/#ansible[Ansible usage in CentOS Infra]

The talk focused on the transition and implementation of Ansible within the CentOS project's infrastructure management.
Initially reliant on manual configurations and Puppet for automation, the CentOS team moved to Ansible due to its simplicity and agentless nature.
This shift allowed for a modular and reusable infrastructure, with careful separation of code, variables, and environments.
The adoption of Ansible roles and collections facilitated better management, testing in staging environments before production, and secure handling of secrets through tools like Git-crypt and Ansible Vault.
Challenges such as maintaining up-to-date Ansible roles and optimizing performance with plugins like Mitogen were addressed.
The talk underscored the evolution from manual management to an efficient, automated system with Ansible at the core, highlighting the project's growth and the benefits of this transition.

==== https://connect.centos.org/#integration[CentOS Integration SIG: current state and future plans]

The talk discusses the objectives and ongoing projects of the CentOS Integration Special Interest Group.
The group focuses on ensuring that services, applications, and systems built on CentOS Stream remain compatible and functional through updates.
Key areas of work include:

* Documentation: Creating and maintaining documentation for integration practices and infrastructure within CentOS.
* Test Management Tool (https://tmt.readthedocs.io/en/stable/[tmt]): Implementing tmt to facilitate distribution-agnostic integration testing, making it easier to maintain compatibility across updates for CentOS, RHEL, Fedora, AlmaLinux, and Rocky.
* GitLab Webhooks: Developing a webhook for CentOS Stream GitLab events to simplify subscription and access to these events.
* Alexandra encourages community participation, from asking questions to improve documentation to contributing test cases, highlighting the inclusive and collaborative nature of the SIG.

==== https://connect.centos.org/#hyperscale[Hyperscale SIG update]

The talk provided an overview of the Hyperscale Special Interest Group's efforts in enhancing CentOS Stream for large-scale infrastructure management.
Key points include automating package backports, developing a Fedora-based hyperscale kernel to facilitate new features, and enhancing user space with a focus on btrfs.
They also introduced live DVD images for easier access to their enhancements.
The SIG encourages open collaboration, offering bi-weekly meetings and monthly socials for community engagement. Future plans include cloud images and a GNOME stack backport from Fedora to CentOS Stream.
During the Q&A session, the speaker mentioned they started using tmt for automated testing.


==== https://connect.centos.org/#hyperscale[State of EPEL]

The talk provided a comprehensive update on the Extra Packages for Enterprise Linux (EPEL) project.
Highlights included the popularity of EPEL 7, the growth of EPEL 9, and an increase in the number of EPEL packages and maintainers.
Troy discussed the scheduled end-of-life for EPEL 7 in June 2024 and touched on future plans for EPEL 8 and the introduction of EPEL 10.
The talk also covered the upcoming EPEL steering committee elections and shared insights from the 2023 EPEL survey.
The presentation underscored EPEL's continued growth and its importance to the Enterprise Linux ecosystem.

==== https://connect.centos.org/#epel10[EPEL 10 Overview]

The talk discusses the evolution of the Extra Packages for Enterprise Linux (EPEL) project, focusing on the forthcoming EPEL 10.
The presentation covers the history of EPEL, challenges encountered in maintaining compatibility with Red Hat Enterprise Linux (RHEL) and CentOS, and the introduction of the "EPEL Next" repository to address these issues.
With EPEL 9, the project shifted to building against CentOS Stream early, resulting in a significant increase in available packages at launch.
For EPEL 10, the plan is to further refine this approach by aligning with RHEL's minor versions, eliminating the need for "EPEL Next," simplifying maintenance, and ensuring broader package compatibility.
The talk emphasizes collaboration and invites community participation in the EPEL project.

=== Friday, February 2

==== https://connect.centos.org/#promodocs[Promo SIG and Docs SIG Updates]

The talk outlines updates from the Promo SIG and Docs SIG within the CentOS community:
==== Promo SIG:

* Focuses on promoting CentOS through events, social media, and the website.
* Organizes significant events and plans to expand presence at other events.
* Manages challenges in social media engagement and forum activity.
* Works on revamping the CentOS website and making budget processes transparent.
==== Docs SIG:

* Recently established to improve CentOS documentation.
* Aims to update the contributor's guide and standardize documentation formats.
* Plans to create a new documentation site using Markdown for ease of contribution.
* Invites community participation in documentation efforts at an upcoming session in Brussels.

Both SIGs highlight the importance of community involvement and the ongoing efforts to enhance visibility and documentation for CentOS.

==== https://connect.centos.org/#packit[Bridging the Gap: Packit automation for CentOS and upstream projects]
The talk highlights Packit as a crucial tool for developers working with CentOS Stream, enabling direct RPM builds from upstream changes and facilitating early issue detection through tmt-based tests.
Additionally, it introduces Testing Farm, showcasing Packit's role in automating the synchronization of upstream releases to Fedora and now to CentOS Stream.
This aims to improve collaboration and streamline the development process for more reliable software delivery.

==== https://connect.centos.org/#secrets[Open Secrets of CentOS Stream]

The talk delves into CentOS Stream's role as an upstream source for RHEL, highlighting its rolling release model that foregoes traditional versioned releases for continuous updates.
It emphasizes the project's commitment to transparency, with public access to composition processes, release engineering code, and development tools.
Additionally, the speaker addresses the switch from Bugzilla to Jira for issue tracking and clarifies misconceptions about the distribution's release strategy.
The presentation underscores CentOS Stream's dynamic nature and its open, community-driven development approach.

==== https://connect.centos.org/#selfabolition[The self-abolition of Enterprise Linux Distributions]

The talk addresses the challenges faced by enterprise Linux distributions in adapting to modern software development and deployment practices.
He contrasts the past, where servers and software had long lifecycles and were manually managed, with the present, characterized by automation, rapid development cycles, and the prevalence of containerization.
Dan highlights issues such as dependency management complexity and the inadequacy of traditional enterprise distributions to support fast-evolving software needs.
He critiques efforts like modularity for failing to solve these problems due to the interdependence of software packages.

Dan proposes focusing on development tools, curating critical software packages, and adopting limited modularity as solutions to make enterprise distributions more appealing and practical for today's software development and deployment requirements.
His talk emphasizes the need for enterprise Linux distributions to evolve and adapt to remain relevant in the rapidly changing technology landscape.

==== https://connect.centos.org/#cloudsig[Cloud SIG Update]

The talk focused on the contributions and future plans of the Cloud Special Interest Group in the open-source cloud infrastructure domain.
The speakers discussed their work on OpenShift and OKD, including maintaining cloud services and enhancing documentation and packaging processes.
They highlighted efforts to welcome new projects and members, detailed the RPM distribution for OpenStack, and introduced new OKD variants based on CentOS Stream.
Future plans involve collaborating for broader image distribution, exploring CoreOS layering for Docker file-based OS manipulation, and extending the utility of their platforms beyond current uses.
The update emphasized the SIG's dedication to supporting cloud infrastructure development and collaboration.


==== https://connect.centos.org/#rdo-okd[OpenStack RDO deployment on Community Distribution of Kubernetes (OKD)]

The talk discusses leveraging Kubernetes and RDO for OpenStack deployments.
Key points include:

* Transition to Kubernetes: Highlighting the shift to Kubernetes for easier management and scalability of OpenStack.
* Challenges with OpenStack: Addressing the complexity of OpenStack's distributed architecture and the move towards a more manageable deployment model.
* Use of Kubernetes Operators: Introducing operators for automating OpenStack deployments within Kubernetes, enhancing efficiency.
* Deployment Tool Introduction: Presenting a new tool to facilitate community deployments and address previous tool deprecations.
* Future Directions: Outlining improvements in documentation, CI support, and container image creation for broader deployment capabilities.

The talk emphasizes the benefits of Kubernetes for OpenStack deployments and invites community feedback and participation.

==== https://connect.centos.org/#mkosi[Building And Utilizing Purpose-Built GNU/Linux Distribution Images Using mkosi]

The talk discusses the benefits and methodology of creating custom Linux distribution images using mkosi.
The speaker, with a background in the Fedora project and Red Hat, explores the need for minimal, tailored distribution images for various applications, contrasting traditional installation methods with the efficiency and customization offered by mkosi.
mkosi allows for the creation of reproducible, bootable images across multiple distributions, emphasizing flexibility through customizable packages, system settings, and the inclusion of custom scripts.
Practical uses in testing, development, education, and immutable infrastructure are highlighted, showcasing mkosi's ability to provide consistent, controlled environments.
The talk concludes with encouragement for community contributions to the mkosi project, including bug reporting, feature suggestions, and use case sharing, underscoring the project's collaborative nature.

==== https://connect.centos.org/#mkosi-hyperscale[Testing the CentOS Hyperscale systemd backport with mkosi]

The talk outlines the process of using mkosi to test the CentOS Hyperscale systemd backport, focusing on the speaker's experiences as a systemd maintainer and their work in the Linux namespace team at Meta.
The goal is to accelerate access to newer systemd versions within CentOS by backporting, necessitating thorough testing beyond basic development environments.
mkosi facilitates this by enabling rapid generation of VM images with the systemd version in question, supporting incremental builds and caching to speed up testing cycles.

The presentation details mkosi's capabilities in handling RPM builds, managing dependencies, and customizing systemd packages for testing.
It also addresses the challenges of ensuring compatibility with SELinux policies when backporting systemd.
A demonstration highlights mkosi's efficiency in building, testing, and deploying systemd changes, emphasizing its role in streamlining the development workflow for the CentOS Hyperscale systemd backport.
The talk concludes with an invitation for questions, aiming to engage the audience in a discussion on systemd development and testing strategies

==== https://connect.centos.org/#sigs-kernel[How SIGs can facilitate contributions to the CentOS Stream kernel]

The talk covers the process of contributing to the CentOS Stream kernel, highlighting the complexities of integrating newer kernel features into a stable base.
The speaker emphasizes the hybrid nature of the CentOS Stream kernel, the challenges in maintaining alternative kernels, and the importance of upstream practices.
Practical advice is given on managing contributions through tools like GitLab, and the role of Continuous Integration and bots in streamlining the process.
The speaker advocates for better support and recognition of alternative enterprise kernels within CentOS Stream, suggesting improved communication and more inclusive contribution processes.


==== https://connect.centos.org/#almalinux[AlmaLinux: How we automated testing without inventing the wheel and instead improving it]

The talk detailed AlmaLinux's approach to enhancing their testing automation by leveraging and contributing to existing tools like OpenQA and Testinfra, rather than starting from scratch.
They tackled challenges such as adding support for enterprise virtualization and improving testing on ppc64 and s390x architectures.
By adopting Fedora's tests for their needs and planning future expansions for migration testing and documentation, AlmaLinux demonstrated a commitment to collaborative improvement and efficiency in their testing processes.

==== https://connect.centos.org/#freeipa[Discuss your identity: how FreeIPA helps running CentOS community infrastructure]

The presentation covers how FreeIPA is used within the CentOS community infrastructure for identity, authentication, and access management, detailing its integration with systems like Fedora's Noggin and Keycloak.
It addresses challenges in authentication, technical upgrades, and the impact of security updates.
Future enhancements in FreeIPA, such as support for identity provider logins, FIDO2 tokens, and certificate management, are also highlighted.
The talk concludes with an offer to help the community adopt these new features to enhance security and efficiency.

== FOSDEM

=== Saturday, February 3

==== https://fosdem.org/2024/schedule/event/fosdem-2024-2352-stopping-all-the-attacks-before-they-start-building-a-security-first-api/[Stopping all the attacks before they start: Building a security-first API]
Overview of common API threats, how to embrace a security-first mindset.

==== https://fosdem.org/2024/schedule/event/fosdem-2024-1710-deploy-fast-without-breaking-things-level-up-apiops-with-opentelemetry/[Deploy Fast, Without Breaking Things: Level Up APIOps With OpenTelemetry ]
Interesting approach to deploying and managing API in a DevOps style.

==== https://fosdem.org/2024/schedule/event/fosdem-2024-2163-evolving-your-apis-a-step-by-step-approach/[Evolving your APIs, a step-by-step approach]
Showcase of APISIX API gateway, what can it do, and why you need it.

==== https://fosdem.org/2024/schedule/event/fosdem-2024-2092-the-d-programming-language-for-modern-open-source-development/[The D Programming Language for Modern Open Source Development]
An interesting language that executes code in compile-time to increase performance in run-time.

==== https://fosdem.org/2024/schedule/event/fosdem-2024-3648-privacy-respecting-usage-metrics-for-free-software-projects/[Privacy-respecting usage metrics for free software projects]
How to collect telemetry in a good way.

==== https://fosdem.org/2024/schedule/event/fosdem-2024-3062-i-want-my-own-cellular-network-having-fun-with-lte-networks-and-open5gs-/[I want my own cellular network! Having fun with LTE networks and Open5Gs]
Overview of how LTE networks work and how to create your own.

==== https://fosdem.org/2024/schedule/event/fosdem-2024-2603-fighting-cancer-with-rust/[Fighting cancer with Rust]
Overview of rust architecture in medical research center.

=== Sunday, February 4

==== https://fosdem.org/2024/schedule/event/fosdem-2024-2949-enhancing-linux-accessibility-a-unified-approach/[Enhancing Linux Accessibility: A Unified Approach]
Overview of Linux usage challenges by visually impaired people.

==== https://fosdem.org/2024/schedule/event/fosdem-2024-2995-the-state-of-enterprise-linux-2024/[The State of Enterprise Linux 2024]
Discussion of problems in enterprise Linux space.

==== https://fosdem.org/2024/schedule/event/fosdem-2024-3104-live-coding-music-with-microblocks-and-microcontrollers-/[Live coding music with MicroBlocks and microcontrollers]
Make music with a block programming language and microcontrollers.

==== https://fosdem.org/2024/schedule/event/fosdem-2024-2881-upstream-and-downstream-best-friends-forever-/[Upstream and downstream, best friends forever?]
Discussion on why upstream and downstream have a backlash and how to solve it.

==== https://fosdem.org/2024/schedule/event/fosdem-2024-2083-generating-music-with-open-tools-apis-and-no-ai-/[Generating music with Open tools, APIs, and NO AI!]
How to make experimental algorithmic music with prime numbers, fractals, and minute time shifts of two samples.
