# -*- coding: utf-8 -*-
import os
import sys

MODULE = "ROOT"
RFD_DIR = f"modules/{MODULE}/pages/rfd"
NAV_FILE = f"modules/{MODULE}/partials/rfd.adoc"


def extract_title(full_path: str, item: str) -> str:
    with open(full_path, 'r', encoding='utf-8') as f:
        # Extract the title of the document
        return next((line[2:].strip() for line in f if line.startswith("= ")), item)

def process_directory(directory: str) -> list[str]:
    output: list[str] = []

    for rfd in sorted(os.listdir(directory)):
        full_path = os.path.join(directory, rfd)

        title = extract_title(full_path, rfd)
        relative_path = os.path.relpath(full_path, RFD_DIR)
        output.append(f"** xref:rfd/{relative_path}[{title}]")

    return output


with open(NAV_FILE, 'w') as nav_file:
    nav_file.write("* RFDs\n")
    for line in process_directory(RFD_DIR):
        nav_file.write(line + "\n")


print(f"Navigation generated in {NAV_FILE}")
