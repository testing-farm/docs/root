# -*- coding: utf-8 -*-
import os
import sys
import re

def generate_nav(basedir, subdir, nav_file_path):
    root_dir_count = basedir.count('/')
    with open(nav_file_path, 'w') as nav_file:
        for root, dirs, files in sorted(os.walk(subdir)):
            # Keep track of how indented the items should be
            dir_count = root.count('/') - root_dir_count
            # Add a parent item, if there is an adoc or not
            dir_name = os.path.basename(root.rstrip(os.sep))
            dir_doc = root + ".adoc"
            if not os.path.exists(dir_doc):
                nav_file.write(f"{(dir_count - 1) * '*'}* {re.sub(r'[-_]', ' ', dir_name).title()}\n")
            else:
                nav_file.write(f"{(dir_count - 1) * '*'}* xref:{dir_name + '.adoc'}[]\n")
            # List all the adocs
            for file in sorted(files):
                if file.endswith(".adoc"):
                    file_path = os.path.join(root, file)
                    relative_path = os.path.relpath(file_path, basedir).replace("\\", "/")
                    nav_file.write(f"{dir_count * '*'}* xref:{relative_path}[]\n")

path = "modules/ROOT/pages"
for d in os.listdir(path):
    subdir = os.path.join(path, d)
    if os.path.isdir(os.path.join(path, d)):
        partial = os.path.basename(subdir.rstrip(os.sep))
        navpath = f"modules/ROOT/partials/{partial}.adoc"
        generate_nav(path, subdir, navpath)
