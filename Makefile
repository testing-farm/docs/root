.DEFAULT_GOAL := help
.PHONY: clean help generate serve

GENERATED_ROOT ?= public
RFD_DIR = modules/ROOT/pages/rfd
SERVE_PORT ?= 8090
SERVE_DIR ?= public

# Help prelude
define PRELUDE

Usage:
  make [target]

endef

##@ Docs

generate: install/npm  ## Generate documentation
	python3 scripts/generate_navigation.py # must be run before generate-rfd-navigation.py or it will recreate the rfd partial. TODO combine/integrate
	rm -rf $(RFD_DIR) && mkdir -p $(RFD_DIR)
	curl -s "https://gitlab.com/api/v4/projects/14474455/repository/archive.tar.gz?path=rfd" | tar -C $(RFD_DIR) -xzv --strip-components 2 -f -
	mv $(RFD_DIR)/*.png modules/ROOT/images
	python3 scripts/generate-rfd-navigation.py
	npx antora --fetch --cache-dir .cache/antora --attribute page-pagination= --redirect-facility gitlab --to-dir $(GENERATED_ROOT) antora-playbook.yml

serve: install/rpm generate  ## Serve documentation on localhost, rebuild automatically on changes
	@echo "[+] Serving docs on http://localhost:$(SERVE_PORT)/Testing%20Farm/0.1"
	# No kill of the background job needed, the process dies after make finished
	@( \
		inotifywait -qmr -e modify,create,delete modules | while read event; do \
			echo $$(date +'%F %T') $event; \
			npx antora --fetch --cache-dir .cache/antora --attribute page-pagination= --redirect-facility gitlab --to-dir $(GENERATED_ROOT) antora-playbook.yml; \
		done; \
	) & \
	python3 -m http.server -d public $(SERVE_PORT)

##@ Utility

install/rpm:  ## Install system dependencies
	@if ! rpm --quiet -q inotify-tools python3; then \
		echo "[+] Installing required system dependencies"; \
		sudo dnf -y install inotify-tools python3; \
	fi

install/npm:  ## Install JS dependencies
	npm install

clean:  ## Cleanup
	rm -rf .cache
	rm -rf public

# See https://www.thapaliya.com/en/writings/well-documented-makefiles/ for details.
reverse = $(if $(1),$(call reverse,$(wordlist 2,$(words $(1)),$(1)))) $(firstword $(1))

help:  ## Show this help
	@awk 'BEGIN {FS = ":.*##"; printf "$(info $(PRELUDE))"} /^[a-zA-Z_/-]+:.*?##/ { printf "  \033[36m%-35s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(call reverse, $(MAKEFILE_LIST))
